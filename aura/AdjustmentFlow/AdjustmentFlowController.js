({
	doInit: function(component, event, helper) 
    {
            
       // Set the validate attribute to a function that includes validation logic
       component.set('v.validate', function() 
       {
           var userInputSubject = component.get('v.objAdjustment');
           //if(userInputSubject && userInputSubject.Trace_Number__c.length>0) 

           if(userInputSubject) 
           {
               //
               var strTrace_Number__c = userInputSubject.Trace_Number__c;
               if (strTrace_Number__c)
               {
                   if (strTrace_Number__c.length>0)
                   {
                       //return { isValid: true };               
                   }
                   else
                   {
                       return { isValid: false, errorMessage: 'A trace number is required.' };
                   }
               }
               else
               {
                      return { isValid: false, errorMessage: 'A trace number is required.' };
               }
               //
               var strDraft_Number__c = userInputSubject.Draft_Number__c;
               if (strDraft_Number__c)
               {
                   if (strDraft_Number__c.length>0)
                   {
                       //return { isValid: true };               
                   }
                   else
                   {
                       return { isValid: false, errorMessage: 'A draft number is required.' };
                   }
               }
               else
               {
                      return { isValid: false, errorMessage: 'A draft number is required.' };
               }
               //
               var strAccount_Number__c = userInputSubject.Account_Number__c;
               if (strAccount_Number__c)
               {
                   if (strAccount_Number__c.length>0)
                   {
                       //return { isValid: true };               
                   }
                   else
                   {
                       return { isValid: false, errorMessage: 'An account number is required.' };
                   }
               }
               else
               {
                      return { isValid: false, errorMessage: 'An account number is required.' };
               }
               //
               var strAmount__c = userInputSubject.Amount__c;
               if (strAmount__c)
               {
                   if (strAmount__c > 0)
                   {
                       //return { isValid: true };               
                   }
                   else
                   {
                       return { isValid: false, errorMessage: 'An amount is required.' };
                   }
               }
               else
               {
                      return { isValid: false, errorMessage: 'An amount is required.' };
               }
				// made it here, should be ok
				return { isValid: true };               

           }
           else 
           {
               // If the component is invalid...
               return { isValid: false, errorMessage: 'A value is required.' };
           }

       })

       
       //Date_Item_Cleared__c

 


    },
})