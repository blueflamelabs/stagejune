({
	getSubCategorys : function(component, event, helper) 
    {
        var action = component.get("c.getStates");
        action.setParams({ "selectedCountry" : component.get("v.Category"),
                          "objectAPIName" : component.get("v.objectAPIName"),
                          "countryPicklistAPIName" : component.get("v.CategoryPicklistAPIName"),
                          "statePicklistAPIName" : component.get("v.SubCategoryPicklistAPIName")
                         });
        action.setCallback(this, function(response) 
       {
            var state = response.getState();
            if (state === "SUCCESS") {
                var stateList = response.getReturnValue();
                component.set("v.SubCategorys", stateList);
                //var lst = component.get("v.SubCategorys");
               // if (lst.size > 0)
                //{
                //	component.set("v.varShowSubCategory", true);
                //}
                //else
                //{
                //	component.set("v.varShowSubCategory", false);
                    
                //}

            }
            else if (state === "ERROR") {
                console.error("Error calling action: "+ response.getState());
            }
        });
        $A.enqueueAction(action);
    }, 
    
   getCategorys : function(component, event, helper) 
    {
        var action = component.get("c.getStates");
        action.setParams({ "selectedCountry" : component.get("v.ServiceArea"),
                          "objectAPIName" : component.get("v.objectAPIName"),
                          "countryPicklistAPIName" : component.get("v.ServiceAreaPicklistAPIName"),
                          "statePicklistAPIName" : component.get("v.CategoryPicklistAPIName")
                         });
        action.setCallback(this, function(response) 
       {
            var state = response.getState();
            if (state === "SUCCESS") {
                var stateList = response.getReturnValue();
                component.set("v.Categorys", stateList);
            }
            else if (state === "ERROR") {
                console.error("Error calling action: "+ response.getState());
            }
        });
        $A.enqueueAction(action);
    }
})