({
     /* 190619 - T - 00185 - to get the owner of the file.*/
	fileReviewForUser : function(cmp, event, helper){
            var fileOwner = cmp.get("v.file.OwnerId");
            console.log('--fileOwner--',fileOwner);
                
            var action = cmp.get("c.getfileUse");
        
        action.setParams({
                'fileOwnerId' : fileOwner
                });
        
        action.setCallback(this,(response)=>{
            const state = response.getState();
            console.log(state,response.getReturnValue());
                if(state === 'SUCCESS'){
                	var response = response.getReturnValue();
                	console.log('reponse--',response);
            		cmp.set("v.fileReviewUser",response);
                }
        });
        
        $A.enqueueAction(action);
      },
})