({
	helperMethod : function() 
    {
		
	},
 
    GetContactId: function(component)
    {
        
        var varBatch = component.get("v.objBatch");            
        component.set("v.varContactId", varBatch.Person_Requesting_Action__c);

    },
    
    GetContactInfo: function(component) 
    {
        var action = component.get("c.fetchContact");
        var varContactId = component.get("v.varContactId");
        action.setParams(
        {                
        	"objContactId" : varContactId                
        });
        
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
               // set current user information on userInfo attribute
                component.set("v.objContact", storeResponse);
                var varContact = component.get("v.objContact");
                //var varFullName = storeResponse.FirstName + ' ' + storeResponse.LastName; 	
                var varFullName = varContact.Name; 	
                component.set("v.objPersonRequestingAction",varFullName );
                
                // leave this here to initize this field , will not work in init function
                var varBatch = component.get("v.objBatch");            
       			varBatch.Action_Requested__c = 'Batch Delete';
               	component.set("v.objBatch", varBatch);
            }
        });
        $A.enqueueAction(action);
	},

    GetContactAccount: function(component) 
    {
        var action = component.get("c.fetchContactAccount");
        var varContactId = component.get("v.varContactId");
        action.setParams(
        {                
        	"objContactId" : varContactId                
        });

        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
               var storeResponse = response.getReturnValue();
               // set current user information on userInfo attribute
                component.set("v.objAccount", storeResponse);
                var varAccount = component.get("v.objAccount");

                //var storeResponse = response.getReturnValue().Name;
               // set current user information on userInfo attribute
                component.set("v.objInstitutionName", varAccount.Name);
            }
        });
        $A.enqueueAction(action);
	},

    GetUserInfo: function(component) 
    {
        var action = component.get("c.fetchUser");
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
               // set current user information on userInfo attribute
                component.set("v.userInfo", storeResponse);
                var varUser = component.get("v.userInfo");
                //var varFullName = storeResponse.FirstName + ' ' + storeResponse.LastName; 	
                var varFullName = varUser.Name; 	
                component.set("v.objPersonRequestingAction",varFullName );
                
                // leave this here to initize this field , will not work in init function
                var varBatch = component.get("v.objBatch");            
       			varBatch.Action_Requested__c = 'Batch Delete';
               	component.set("v.objBatch", varBatch);
            }
        });
        $A.enqueueAction(action);
	},
    
    GetUserAccount: function(component) 
    {
        var action = component.get("c.fetchUserAccount");
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue().Name;
               // set current user information on userInfo attribute
                component.set("v.objInstitutionName", storeResponse);
            }
        });
        $A.enqueueAction(action);
	},
    
    GetSubmissionDeadlinePassed: function(component) 
    {
        //initial
        var bPassed = true;
        component.set("v.SubmissionDeadlinePassed", bPassed);
        
        var action = component.get("c.GetECCSubmissionDeadlinePassed");
        action.setCallback(this, function(response) 
        {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
               // set current user information on userInfo attribute
                component.set("v.SubmissionDeadlinePassed", storeResponse);
            }
        });
        $A.enqueueAction(action);
	},
   
    


})