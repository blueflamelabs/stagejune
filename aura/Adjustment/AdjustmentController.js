({
	    // Load  from Salesforce
     doInit: function(component, event, helper) 
    {
            var varSubCategory = component.get("v.SubCategory");
            
            if (varSubCategory == 'Encoding Errors')
            {
          		component.find("CorrectAmount").set("v.disabled", false);
            }
        	else
            {
          		component.find("CorrectAmount").set("v.disabled", true);
            }
            
            if (varSubCategory == 'Duplicate Items Paid')
            {
          		component.find("OtherClearDate").set("v.disabled", false);
            }
        	else
            {
          		component.find("OtherClearDate").set("v.disabled", true);
            }
            

    },
    // 
     onChangeSubCategory: function(component, event, helper) 
    {
            var varSubCategory = component.get("v.SubCategory");
            
            if (varSubCategory == 'Encoding Errors')
            {
          		component.find("CorrectAmount").set("v.disabled", false);
            }
        	else
            {
          		component.find("CorrectAmount").set("v.disabled", true);
            }
            
            if (varSubCategory == 'Duplicate Items Paid')
            {
          		component.find("OtherClearDate").set("v.disabled", false);
            }
        	else
            {
          		component.find("OtherClearDate").set("v.disabled", true);
            }

            

    },
})