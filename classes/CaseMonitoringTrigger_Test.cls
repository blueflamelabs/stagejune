/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy     Description
*     1.0        06252019         Users     This class is built to test Case MonitoringTrigger apex trigger
**********************************************************************************************************************************************************/ 
@isTest
public class CaseMonitoringTrigger_Test 
{
    @isTest
    public static void AfterUpdateAdminTest() 
    {
           
        UserRole ur = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role',PortalType = 'None');
        insert ur;
        
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        
        User user;
        Account ac;
        Contact con;
        
        User portalAccountOwner1 = new User(
            UserRoleId = ur.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test1111112@test.com',
            Alias = 'batman',
            Email='bruce.wayne11111@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce111111',
            Lastname='Wayne11111',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        insert portalAccountOwner1 ;

            System.runAs(portalAccountOwner1)
            {
            test.startTest();
    
        Account accObj = new Account();
            accObj.Name = 'Test Accountddddd';
            
            insert accObj;
            
            Contact objContact = new Contact();
            objContact.FirstName = 'Test';
            objContact.LastName = 'Contactddddddd';
            objContact.AccountId = accObj.Id;
            objContact.Email = 'tesdddddddt@gmail.com';
            
            insert objContact;
            
            Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('External Service Request').getRecordTypeId();
            Case caseObj = new Case();
            caseObj.Subject = 'Test Case Subjectddddd';
            caseObj.Description = 'Test Case Description';
            caseObj.Status = 'New';
            caseObj.Origin = 'Email';
            caseObj.Date_Required__c = Date.today();
            caseObj.ContactId = objContact.Id;
            caseObj.recordtypeid = RecordTypeIdCase;
            
            insert caseObj;
            
            
            
                syn_Case_Comment__c caseComment = new syn_Case_Comment__c();
                caseComment.Name = 'Test';
                caseComment.IsPublished__c = true;
                caseComment.Review_Complete__c = false;
                caseComment.Case__c = caseObj.Id;
            	
        		insert caseComment;

                    List<Case_Monitoring__c> caseMonitoringRecs = [SELECT Id, Review_Complete__c, Acknowledge_Date__c FROM Case_Monitoring__c WHERE Parent_Id__c = :caseComment.Id];
        	 List<Case> lstcaseRecs = [SELECT Id, Case_Communication_Flag__c FROM Case WHERE Id = :caseObj.Id];
            
                // admin user
                system.assertEquals(true, caseMonitoringRecs.size() == 1);
                system.assertEquals(true, caseMonitoringRecs[0].Review_Complete__c == true);
               system.assertEquals(true, lstcaseRecs[0].Case_Communication_Flag__c == false);
                
                
                 //caseComment.Review_Complete__c = false;
                //update caseComment;

                // caseComment.Review_Complete__c = true;
                //update caseComment;
                 caseMonitoringRecs[0].Review_Complete__c = false;
                update caseMonitoringRecs[0];

                 caseMonitoringRecs[0].Review_Complete__c = true;
                update caseMonitoringRecs[0];
                
                caseMonitoringRecs[0].Review_Complete__c = false;
                update caseMonitoringRecs[0];

                Test.stopTest();

            
        }

    }


}