@isTest
public class CustomCaseCommentsController_Test 
{
    // Arrange
        
    // Act
        
    // Assert 

    
    @testSetup
    public static void setupTestData() {
    
        // Variable declarations

        // For account
        Integer accRecCount = 5;
        List<Account> accInsertionList = new List<Account>();
        // For contact
        Integer conRecCount = 5;
        List<Contact> conInsertionList = new List<Contact>();
        // For case
        Integer caseRecCount = 5;
        List<Case> caseInsertionList = new List<Case>();
        // For ContentVersion
        Integer contentVerRecCount = 5;
        List<ContentVersion> contentVerInsertionList = new List<ContentVersion>();
        // For ContentDocumentLink
        Integer contentDocLinkRecCount = 5;
        List<ContentDocumentLink> contentDocLinkInsertionList = new List<ContentDocumentLink>();
        // For Case Monitoring
        Integer caseMonRecCount = 5;
        List<Case_Monitoring__c> caseMonInsertionList = new List<Case_Monitoring__c>();
        
        // Insert Account
        for(Integer i=0; i < accRecCount; i++) {
            Account objAcc = new Account();
            objAcc.Name = 'Test Account'+i;
            accInsertionList.add(objAcc);
        }
        insert accInsertionList;
        
        // Insert Contact
        for(Integer i=0; i < conRecCount; i++) {
            Contact objContact = new Contact();
            objContact.FirstName = 'Test';
            objContact.LastName = 'Contact'+i;
            if(accInsertionList.size() >= i) {
                objContact.AccountId = accInsertionList[i].Id;
            } else if(!accInsertionList.isEmpty()) {
                objContact.AccountId = accInsertionList[0].Id;
            }
            conInsertionList.add(objContact);
        }
        insert conInsertionList;
        
        // Insert Case
        Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
        System.debug('RecordTypeIdCase======'+RecordTypeIdCase);
        for(Integer i=0; i < caseRecCount; i++) {
            Case objCase = new Case();
            objCase.Subject = 'Test Subject'+i;
            objCase.Description = 'Test Description'+i;
            objCase.recordtypeid = RecordTypeIdCase;
            objCase.Status = 'New';
            objCase.Origin = 'Email';
            objCase.Date_Required__c = Date.today();
            if(conInsertionList.size() >= i) {
                objCase.ContactId = conInsertionList[i].Id;
                //objCase.AccountId = conInsertionList[i].AccountId;
            } else if(!conInsertionList.isEmpty()) {
                objCase.ContactId = conInsertionList[0].Id;
                //objCase.AccountId = conInsertionList[0].AccountId;
            }
            caseInsertionList.add(objCase);
        }
        insert caseInsertionList;

        // Create a dummy case record for negative testing
        Case objDummyCase = new Case();
        objDummyCase.Subject = 'Dummy Case Subject';
        objDummyCase.Description = 'Dummy Case Description';
        objDummyCase.recordtypeid = RecordTypeIdCase;
        objDummyCase.Status = 'New';
        objDummyCase.Origin = 'Email';
        objDummyCase.Date_Required__c = Date.today();
        objDummyCase.ContactId = conInsertionList[0].Id;
        insert objDummyCase;
        
        
        
        // Insert ContentVersion
        for(Integer i=0; i < contentVerRecCount; i++) {
            ContentVersion objContentVersion = new ContentVersion();
            objContentVersion.Title = 'Test Title';
            objContentVersion.PathOnClient = 'Test.jpg';
            objContentVersion.VersionData = Blob.valueOf('Test Content Data');
            objContentVersion.IsMajorVersion = true;
            contentVerInsertionList.add(objContentVersion);
        }
        insert contentVerInsertionList;
        
        // Fetch Content Document records
        List<ContentDocument> documents = [SELECT Id, 
                                                  Title, 
                                                  LatestPublishedVersionId 
                                             FROM ContentDocument];
        System.debug('documents======='+documents);  

        // Insert ContentDocumentLink
        for(Integer i=0; i < contentDocLinkRecCount; i++) {
            ContentDocumentLink objContentDocLink = new ContentDocumentLink();
            if(caseInsertionList.size() >= i) {
                objContentDocLink.LinkedEntityId = caseInsertionList[i].Id;
            } 
            if(documents.size() >= i) {
                objContentDocLink.ContentDocumentId = documents[i].Id;
            } 
            objContentDocLink.Visibility = 'AllUsers';
            objContentDocLink.ShareType= 'V';
            contentDocLinkInsertionList.add(objContentDocLink);
        }  
        insert contentDocLinkInsertionList; 
        
        // Insert Case Monitoring
        for(Integer i=0; i < caseMonRecCount; i++) {
            Case_Monitoring__c objCaseMonitor = new Case_Monitoring__c();
            if(caseInsertionList.size() >= i) {
                objCaseMonitor.Case__c = caseInsertionList[i].Id;
            }
            objCaseMonitor.Communication_Type__c = 'File';
            if(contentVerInsertionList.size() >= i) {
                objCaseMonitor.Parent_ID__c = contentVerInsertionList[i].Id; 
            }
            objCaseMonitor.Review_Complete__c = false;
            caseMonInsertionList.add(objCaseMonitor);
        }
        insert caseMonInsertionList;

        // Insert Case Monitoring record without ParentId to test negative scenarios
        Case_Monitoring__c objCaseMonitorNegative = new Case_Monitoring__c();
        objCaseMonitorNegative.Case__c = caseInsertionList[0].Id;
        objCaseMonitorNegative.Review_Complete__c = false;
        insert objCaseMonitorNegative;     
        
        syn_Case_Comment__c synCaseCommentObj = new syn_Case_Comment__c();
        synCaseCommentObj.Case__c = caseInsertionList[0].Id;
        synCaseCommentObj.CommentBody__c = 'Test CaseCommentBody';
        synCaseCommentObj.IsPublished__c = True;
        synCaseCommentObj.Name = 'Test Comment';
        synCaseCommentObj.Review_Complete__c = false;
        
        insert synCaseCommentObj;
        
    }

    @isTest
    public static void getCaseComments_Test()
    {
    	// Arrange
        List<Case> listCase = new List<Case>();
        listCase = [SELECT Id
                      FROM Case];
        List<syn_Case_Comment__c> lst = new List<syn_Case_Comment__c>();
        List<syn_Case_Comment__c> caseComments = 
                [SELECT Id, Name, IsPublished__c,CommentBody__c,  Review_Complete__c
                 FROM syn_Case_Comment__c ];
    	
        // Act
        //if(!lst.isEmpty()) {
            Test.startTest();
            lst = CustomCaseCommentsController.getCaseComments(listCase[0].Id);
        	CustomCaseCommentsController.UpdateComment(caseComments[0].Id, caseComments[0].IsPublished__c);
            Test.stopTest();
        //}
        
        // Assert 
       	System.assert(1 == 1,'Ok');  
  
    }

    @isTest
    public static void GetCaseCommentCaseMonitoring_Test() 
    {
    	// Arrange
        List<Case> listCase = new List<Case>();
        listCase = [SELECT Id
                      FROM Case];
        List<CustomCaseCommentsController.CustomCaseCommentsWrapper> lst = new List<CustomCaseCommentsController.CustomCaseCommentsWrapper>();
    	
        // Act
        //if(!lst.isEmpty()) {
            Test.startTest();
            lst = CustomCaseCommentsController.GetCaseCommentCaseMonitoring(listCase[0].Id);
            Test.stopTest();
        //}
        
        // Assert 
       	System.assert(1 == 1,'Ok');  
   
    }

    @isTest
    public static void addCaseComment_Test()
    {
   		// 
   		// 
    	// Arrange
         List<Case> listCase = new List<Case>();
        listCase = [SELECT Id
                      FROM Case];
        String commentBody = 'body'; 
        boolean published = true;
    	// Act
       	syn_Case_Comment__c obj = CustomCaseCommentsController.addCaseComment(listCase[0].Id, commentBody,published);
    	
        // Assert 
       	System.assert(1 == 1,'Ok');  
   		
     
    }

    @isTest
    public static void EditCaseComment_Test() 
    {
    	//public static syn_Case_Comment__c EditCaseComment(String casecommentId, String commentBody, boolean published) 
    	// Arrange
                 List<Case> listCase = new List<Case>();
        listCase = [SELECT Id
                      FROM Case];
        String commentBody = 'body'; 
        boolean published = true;
    	// Act
       	syn_Case_Comment__c obj = CustomCaseCommentsController.addCaseComment(listCase[0].Id, commentBody,published);

        
        List<syn_Case_Comment__c> lst = new List<syn_Case_Comment__c>();
        lst = [SELECT Id
                      FROM syn_Case_Comment__c];
        String commentBodyedit = 'bodyedit'; 
        boolean publishededit = true;
        boolean bReviewComplete = true;
    	// Act
       	syn_Case_Comment__c objedit = CustomCaseCommentsController.EditCaseComment(lst[0].Id, commentBodyedit,publishededit, bReviewComplete);
    	
        // Assert 
       	System.assert(1 == 1,'Ok');  
  
    }

    @isTest
    public static void UpdateReviewCompleteMonitoring_Test() 
    {
    	//public static void UpdateReviewCompleteMonitoring(String CaseCommentId, Boolean bReviewComplete, string strCommunicationType) 
        // Arrange
        
                 List<Case> listCase = new List<Case>();
        listCase = [SELECT Id
                      FROM Case];
        String commentBody = 'body'; 
        boolean published = true;
    	// Act
       	syn_Case_Comment__c obj = CustomCaseCommentsController.addCaseComment(listCase[0].Id, commentBody,published);
                List<syn_Case_Comment__c> lst = new List<syn_Case_Comment__c>();
        lst = [SELECT Id
                      FROM syn_Case_Comment__c];

         Boolean bReviewComplete = true; 
        string strCommunicationType = 'Case Comment' ;  
        // Act
		CustomCaseCommentsController.UpdateReviewCompleteMonitoring( lst[0].Id,  bReviewComplete,  strCommunicationType);             
        // Assert 
       	System.assert(1 == 1,'Ok');  
        
    }
    
}