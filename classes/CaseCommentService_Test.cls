/*********************************************************************************************************************************************************
*     Version    CreateDate     CreatedBy     Description
*     1.0        180619         BFL Users     This class is built to test CaseCommentService apex class
**********************************************************************************************************************************************************/ 
@isTest
public class CaseCommentService_Test {
    
    /**
    * Method Name : positiveTestAfterInsert
    * Parameters  : none
    * Description : This method is used to test positive scenario of send mail functionality for internal users.
    **/
    @isTest
    public static void positiveTestAfterInsert() {
        
        List<User> internalUsers = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND ContactId = null AND IsActive = true];
        Test.startTest();
        
        System.runAs(internalUsers[0]){
            
            Email_Notification_Trigger_Setting__c custObj = new Email_Notification_Trigger_Setting__c();
            custObj.Is_Case_Comment_Notification_Active__c = true;
            custObj.Is_File_Notification_Active__c = false;
            
            insert custObj;
            
            Account accObj = new Account();
            accObj.Name = 'Test Account';
            
            insert accObj;
            
            Contact objContact = new Contact();
            objContact.FirstName = 'Test';
            objContact.LastName = 'Contact';
            objContact.AccountId = accObj.Id;
            objContact.Email = 'test@gmail.com';
            
            insert objContact;
            
            Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
            Case caseObj = new Case();
            caseObj.Subject = 'Test Case Subject';
            caseObj.Description = 'Test Case Description';
            caseObj.Status = 'New';
            caseObj.Origin = 'Email';
            caseObj.Date_Required__c = Date.today();
            caseObj.ContactId = objContact.Id;
            caseObj.recordtypeid = RecordTypeIdCase;
            
            insert caseObj;
            
            
            
            syn_Case_Comment__c caseComment = new syn_Case_Comment__c();
            caseComment.Name = 'Test' ;
            caseComment.IsPublished__c = true;
            caseComment.Case__c = caseObj.Id;
            
            insert caseComment;
            
            CaseCommentHandler.Bypasstrigger = true;
            
            Integer invocations = Limits.getEmailInvocations();
            system.assertEquals(1, invocations);
            
        }
        Test.stopTest();
        
    }
    
    /**
    * Method Name : positiveTestAfterInsert1
    * Parameters  : none
    * Description : This method is used to test positive scenario of send mail functionality for community users.
    **/
    @isTest
    public static void positiveTestAfterInsert1() {
        
        List<User> communityUsers = [SELECT Id FROM User WHERE ContactId != null AND IsActive = true];
        List<NetworkMember> networkMembers = [SELECT Id, MemberId, NetworkId FROM NetworkMember WHERE MemberId = :communityUsers[0].Id];
        Test.startTest();
        
        System.runAs(communityUsers[0]){
            
            Email_Notification_Trigger_Setting__c custObj = new Email_Notification_Trigger_Setting__c();
            custObj.Is_Case_Comment_Notification_Active__c = true;
            custObj.Is_File_Notification_Active__c = false;
            
            insert custObj;
            
            Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
            Case caseObj = new Case();
            caseObj.Subject = 'Test Case Subject';
            caseObj.Description = 'Test Case Description';
            caseObj.Status = 'New';
            caseObj.Origin = 'Email';
            caseObj.Date_Required__c = Date.today();
            caseObj.recordtypeid = RecordTypeIdCase;
            
            insert caseObj;
            
            
            syn_Case_Comment__c caseComment = new syn_Case_Comment__c();
            caseComment.Name = 'Test' ;
            caseComment.IsPublished__c = true;
            caseComment.Case__c = caseObj.Id;
            
            insert caseComment;
            
            CaseCommentHandler.Bypasstrigger = true;
            Integer invocations = Limits.getEmailInvocations();
            system.assertEquals(1, invocations);
        }
        Test.stopTest();
        
    }
    
    /**
    * Method Name : positiveTestAfterUpdate
    * Parameters  : none
    * Description : This method is used to test positive scenario of send mail functionality for internal users.
    **/
    @isTest
    public static void positiveTestAfterUpdate() {
        // query for internal user
        List<User> internalUsers = [SELECT Id FROM User WHERE Profile.Name = 'System Administrator' AND ContactId = null AND IsActive = true];
        Test.startTest();
        
        System.runAs(internalUsers[0]){
            
            // insert custom setting
            Email_Notification_Trigger_Setting__c custObj = new Email_Notification_Trigger_Setting__c();
            custObj.Is_Case_Comment_Notification_Active__c = true;
            custObj.Is_File_Notification_Active__c = false;
            
            insert custObj;
            
            // insert account
            Account accObj = new Account();
            accObj.Name = 'Test Account';
            
            insert accObj;
            
            //insert contact
            Contact objContact = new Contact();
            objContact.FirstName = 'Test';
            objContact.LastName = 'Contact';
            objContact.AccountId = accObj.Id;
            objContact.Email = 'test@gmail.com';
            
            insert objContact;
            
            // insert case
            Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
            Case caseObj = new Case();
            caseObj.Subject = 'Test Case Subject';
            caseObj.Description = 'Test Case Description';
            caseObj.Status = 'New';
            caseObj.Origin = 'Email';
            caseObj.Date_Required__c = Date.today();
            caseObj.recordtypeid = RecordTypeIdCase;
            caseObj.ContactId = objContact.Id;
            
            insert caseObj;
            
            // insert syn_Case_Comment__c record
            syn_Case_Comment__c caseComment = new syn_Case_Comment__c();
            caseComment.Name = 'Test' ;
            caseComment.IsPublished__c = false;
            caseComment.Case__c = caseObj.Id;
            
            insert caseComment;

            caseComment.IsPublished__c = true;
            
            // update syn_Case_Comment__c 
            Update caseComment;
            
            CaseCommentHandler.Bypasstrigger = true;
            
            Integer invocations = Limits.getEmailInvocations();
            system.assertEquals(1, invocations);
            
        }
        Test.stopTest();
        
    }
    
    /**
    * Method Name : TestAfterUpdateForCommunity
    * Parameters  : none
    * Description : This method is used to test positive scenario of send mail functionality for community users.
    **/
    @isTest
    public static void TestAfterUpdateForCommunity() {
       
        // query for community user
       List<User> communityUsers = [SELECT Id FROM User WHERE ContactId != null AND IsActive = true];
        List<NetworkMember> networkMembers = [SELECT Id, MemberId, NetworkId FROM NetworkMember WHERE MemberId = :communityUsers[0].Id];
        Test.startTest();
        
        System.runAs(communityUsers[0]){
            // insert Custom setting
            Email_Notification_Trigger_Setting__c custObj = new Email_Notification_Trigger_Setting__c();
            custObj.Is_Case_Comment_Notification_Active__c = true;
            custObj.Is_File_Notification_Active__c = false;
            
            insert custObj;
            
            // insert case
            Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Internal Service Request').getRecordTypeId();
            Case caseObj = new Case();
            caseObj.Subject = 'Test Case Subject';
            caseObj.Description = 'Test Case Description';
            caseObj.Status = 'New';
            caseObj.Origin = 'Email';
            caseObj.Date_Required__c = Date.today();
            caseObj.recordtypeid = RecordTypeIdCase;
            
            
            insert caseObj;
            
            //insert syn_Case_Comment__c
            syn_Case_Comment__c caseComment = new syn_Case_Comment__c();
            caseComment.Name = 'Test' ;
            caseComment.IsPublished__c = false;
            caseComment.Case__c = caseObj.Id;
            
            insert caseComment;

            caseComment.IsPublished__c = true;
            
            // update syn_Case_Comment__c
            Update caseComment;
            
            CaseCommentHandler.Bypasstrigger = true;
            Integer invocations = Limits.getEmailInvocations();
            system.assertEquals(1, invocations);
            
        }
        Test.stopTest();
    }
    
}