@isTest
private class Test_trgr_SetAttachmentVisibility 
{
	
    static TestMethod  void TestNote()
    {
        
        //String ACCOUNT_ID = '001D000000K2ES6';
        
        Account objTestAccount = new Account();
        objTestAccount.Name = 'TEST_APEX_Account';
        insert objTestAccount;
    
         String ACCOUNT_ID = objTestAccount.Id;
        
        ContentNote n = new ContentNote();
        n.title = 'My Note from Apex';
        insert n;
        
        ContentDocumentLink link = new ContentDocumentLink();
        link.LinkedEntityId = ACCOUNT_ID;
        link.ContentDocumentId = n.id;
        link.ShareType = 'V';
        link.Visibility = 'AllUsers';
        insert link;
      
        System.AssertEquals(1,1);
	}
}