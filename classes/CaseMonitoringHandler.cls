public class CaseMonitoringHandler 
{
    @testVisible static Boolean Bypasstrigger = false;
    
    List<Case_Monitoring__c > contentRecords = new  List<Case_Monitoring__c >();
    
    //  Used to store Triger.oldMap
    Map<Id, Case_Monitoring__c> objOldMap = new Map<Id, Case_Monitoring__c>();
    //  Used to store filtered case comment records
    List<Case_Monitoring__c > filtertedCaseCommentRecords = new List<Case_Monitoring__c >();
    
    public CaseMonitoringHandler (List<sObject> cvRecords)
    {
        contentRecords = (List<Case_Monitoring__c >)cvRecords;
        
    }
    public CaseMonitoringHandler (List<sObject> cvRecords, Map<Id, Case_Monitoring__c> objRecordsOldMap)
    {
        contentRecords = (List<Case_Monitoring__c >)cvRecords;
        objOldMap = objRecordsOldMap;
    }

    
    public void afterUpdate()
    {
        // 190619 - T - 00295 - Added debug
        System.debug('Inside afterUpdate');
        List<Case_Monitoring__c> objLstMonitoring = new List<Case_Monitoring__c>();
    	List<Case> objLstCase = new List<Case>();
    	Case objCase;
    
        for(Case_Monitoring__c obj : contentRecords)
        {
            objLstMonitoring = [SELECT Id, Case__c, Acknowledge_Date__c FROM Case_Monitoring__c WHERE Case__c =: obj.Case__c AND  Review_Complete__c = false AND Acknowledge_Date__c = null];
            objCase = [SELECT Id, CaseNumber, Status, Case_Communication_Flag__c, OwnerId FROM Case WHERE ID =: obj.Case__c ];
            // 190619 - T - 00295 - Added debugs
        	System.debug('objLstMonitoring========'+objLstMonitoring);
            System.debug('objCase========'+objCase);
            System.debug('obj.Review_Complete__c========'+obj.Review_Complete__c);
            if (obj.Review_Complete__c == false)
            {
                // 190619 - T - 00295 - Added debugs
        		System.debug('Inside main if');
                if (objCase.Case_Communication_Flag__c == false)
                {
                    // 190619 - T - 00295 - Added debugs
        			System.debug('Case communication flag is false');
                    objCase.Case_Communication_Flag__c = true;
                    objLstCase.add(objCase);   
                }    
            }
            else
            {
            	// 190619 - T - 00295 - Added debugs
        		System.debug('Inside main else');
                if (objLstMonitoring.size()==0)
                {
                    // 190619 - T - 00295 - Added debugs
        			System.debug('objLstMonitoring is zero');
                    if (objCase.Case_Communication_Flag__c == true)
                    {
                        // 190619 - T - 00295 - Added debugs
        				System.debug('case comm flag is true');
                        objCase.Case_Communication_Flag__c = false;
                        objLstCase.add(objCase);
                    }
                }
                if (objLstMonitoring.size()>0)
                {
                    // 190619 - T - 00295 - Added debugs
        			System.debug('objLstMonitoring is greater than zero');
                    if (objCase.Case_Communication_Flag__c == false)
                    {
                        // 190619 - T - 00295 - Added debugs
        				System.debug('Case_Communication_Flag__c is false');
                        objCase.Case_Communication_Flag__c = true;
                        objLstCase.add(objCase);   
                    }
                }  
            }
         }
    
       // 190619 - T - 00295 - Added debug
       System.debug('objLstCase======'+objLstCase);
       if (objLstCase.size()>0)
       {
             update objLstCase;  
       }
    
	}
    
    
}