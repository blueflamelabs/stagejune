public class ContentVersionHandler {
    @testVisible static Boolean Bypasstrigger = false;
    List<ContentVersion> contentRecords = new  List<ContentVersion>();
    // 250619 - T - 00326 - Used to store the filtered content version records on the basis of file sharing option
    List<ContentVersion> listFilteredContentRecords = new List<ContentVersion>();
    // 250619 - T - 00326 - Used to store the content version records from Trigger.oldMap
    Map<Id, ContentVersion> contentVerionOldMap = new Map<Id, ContentVersion>();
    // 180619 - T - 00275 - Fetch custom setting Org Default value to on/off file upload email notification functionality
    Email_Notification_Trigger_Setting__c triggerSetting = Email_Notification_Trigger_Setting__c.getOrgDefaults();
    
    public ContentVersionHandler (List<sObject> cvRecords){
        contentRecords = (List<ContentVersion>)cvRecords;
    }
    // 250619 - Added to get list of content version records and map of records from Trigger.OldMap
    public ContentVersionHandler(List<sObject> cvRecords, Map<Id, ContentVersion> contentVerionRecordsOldMap)
    {
        contentRecords = (List<sObject>)cvRecords;
        contentVerionOldMap = contentVerionRecordsOldMap;
    }

    public void beforeInsert(){
        if(Bypasstrigger) return;
        //ContentVersionService.fileReviewFlagHandler(contentRecords);
        ContentVersionService.contentDocumentWithCaseMonitoringRecMapping(ContentVersionService.getCaseTypeContentVersionRecords(contentRecords));
    }
    public void afterUpdate(){
         //System.debug('Inside after update');
         //System.debug('ContentVersionService.isInitialUpdate======'+ContentVersionService.isInitialUpdate);
         Boolean isSharingOptionChanged = false;
         if(Bypasstrigger) return;
         ContentVersionService.updateCaseMonitoringRecordWhenFileUpdate(ContentVersionService.getCaseTypeContentVersionRecords(contentRecords));
        //ContentVersionService.updateCaseRecordWhenFileUpdate(contentRecords);

        if(triggerSetting.Is_File_Notification_Active__c && !ContentVersionService.isInitialUpdate) {

            // 250619 - T - 00326 - Filter ContentVersion records on the basis of FileSharing option
            //System.debug('contentVerionOldMap======='+contentVerionOldMap);
            for(ContentVersion objContentVersion : contentRecords) {
                ContentVersion objOldContentVersion = contentVerionOldMap.get(objContentVersion.Id);
                String previousSharingPrivacyOption = objOldContentVersion.SharingPrivacy;
                String currentSharingPrivacyOption = objContentVersion.SharingPrivacy;
                //System.debug('previousSharingPrivacyOption======='+previousSharingPrivacyOption);
                //System.debug('currentSharingPrivacyOption======='+currentSharingPrivacyOption);
                if(previousSharingPrivacyOption != currentSharingPrivacyOption && previousSharingPrivacyOption == 'P' && currentSharingPrivacyOption == 'N') {
                    isSharingOptionChanged = true;
                    listFilteredContentRecords.add(objContentVersion);
                } else if(!objContentVersion.IsEmailNotificationSent__c && previousSharingPrivacyOption == currentSharingPrivacyOption && currentSharingPrivacyOption == 'N') {
                    listFilteredContentRecords.add(objContentVersion);
                }// End of if
            } // End of for

            //System.debug('listFilteredContentRecords in after update======'+listFilteredContentRecords);
            // 250619 - T - 00326 - Call Service Class to send file upload email notifications
            if(!listFilteredContentRecords.isEmpty()) {
                ContentVersionService.sendEmailNotification(listFilteredContentRecords, isSharingOptionChanged);
            }
        } // End of if that is if custom setting for email notification is Active
    }
     public void afterInsert(){
        if(Bypasstrigger) return;
        ContentVersionService.updateCaseMonitoringRecords(ContentVersionService.getCaseTypeContentVersionRecords(contentRecords));
        //ContentVersionService.updateReviewStatus(contentRecords);
        //ContentVersionService.updateCaseRecordWhenFileInsert(contentRecords);
        //ContentVersionService.sendEmailNotification(contentRecords);
        
        // 070619 - T-00209 - Added debug
        // System.debug('ContentVersionHandler called'); 
        // 180619 - T - 00275 - Check if triger setting is active
        if(triggerSetting.Is_File_Notification_Active__c) {

            // 250619 - T - 00326 - Check if triger setting is active
            List<ContentVersion> listContentVersionToUpdate = new List<ContentVersion>();
            // 250619 - T - 00326 - Filter ContentVersion records on the basis of FileSharing option
            for(ContentVersion objContentVersion : contentRecords) {
                
                if(objContentVersion.SharingPrivacy == 'N' && objContentVersion.VersionNumber != '1') {
                    listFilteredContentRecords.add(objContentVersion);
                } // End of if
            } // End of for

            //System.debug('listFilteredContentRecords in after insert======'+listFilteredContentRecords);
            // Call Service Class to send file upload email notifications
            if(!listFilteredContentRecords.isEmpty()) {
                ContentVersionService.sendEmailNotification(listFilteredContentRecords, false);
            } // End of if
        }
    }

    

}