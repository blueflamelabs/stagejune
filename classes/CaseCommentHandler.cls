public class CaseCommentHandler 
{

    @testVisible static Boolean Bypasstrigger = false;
    List<syn_Case_Comment__c > contentRecords = new  List<syn_Case_Comment__c >();
    // 170619 - Used to store Triger.oldMap
    Map<Id, syn_Case_Comment__c> caseCommentOldMap = new Map<Id, syn_Case_Comment__c>();
    // 170619 - Used to store filtered case comment records
    List<syn_Case_Comment__c > filtertedCaseCommentRecords = new List<syn_Case_Comment__c >();
    // 180619 - T - 00275 - Fetch custom setting Org Default value to on/off case comment email notification functionality
    Email_Notification_Trigger_Setting__c triggerSetting = Email_Notification_Trigger_Setting__c.getOrgDefaults();
    
    public CaseCommentHandler (List<sObject> cvRecords)
    {
        contentRecords = (List<syn_Case_Comment__c >)cvRecords;
        
    }
    // 170619 - Added to get list of case comment records and map of records from Trigger.OldMap
    public CaseCommentHandler (List<sObject> cvRecords, Map<Id, syn_Case_Comment__c> caseCommentRecordsOldMap)
    {
        contentRecords = (List<syn_Case_Comment__c >)cvRecords;
        caseCommentOldMap = caseCommentRecordsOldMap;
    }

    public void beforeInsert()
    {
        if(Bypasstrigger) return;
        //CaseCommentService.contentDocumentWithCaseMonitoringRecMapping(CaseCommentService.getCaseTypeCase_CommentRecords(contentRecords));
    }
    public void afterUpdate()
    {        
         if(Bypasstrigger) return;
         // 170619 - Added to check if logged in user is community user or not
         Boolean isCommunityUser = GetIsCommunityUser();
         //System.debug('================afterUpdate called=========');
         //System.debug('isCommunityUser========='+isCommunityUser);
         //System.debug('caseCommentOldMap========='+caseCommentOldMap);

         // 170619 - Filter case comment records on the basis of Public checkbox value
         if(!isCommunityUser) {

            // If logged in user is internal user
            for(syn_Case_Comment__c objCaseComment : contentRecords) {

                // Access the "old" record by its Id in Trigger.oldMap
                syn_Case_Comment__c objOldCaseComment = caseCommentOldMap.get(objCaseComment.Id);
                Boolean oldIsPublic = objOldCaseComment.IsPublished__c;
                Boolean newIsPublic = objCaseComment.IsPublished__c;
                // Filter case comment records if previous public flag was false and updated is true
                if(newIsPublic && !oldIsPublic) {

                    filtertedCaseCommentRecords.add(objCaseComment);
                } // End of if
            } // End of for

            // Send case comment records to service class only if public checkbox is updated from false to true
            if(!filtertedCaseCommentRecords.isEmpty()) {

                // 180619 - T - 00275 - Check if triger setting is active
                if(triggerSetting.Is_Case_Comment_Notification_Active__c) {

                    // Pass filtered case comment records
                    CaseCommentService.sendEmailNotificationOnCaseComment(filtertedCaseCommentRecords);
                }
            } // End of if
         } else {

            // 180619 - T - 00275 - Check if triger setting is active
            if(triggerSetting.Is_Case_Comment_Notification_Active__c) {

                // Pass all case comment records
                CaseCommentService.sendEmailNotificationOnCaseComment(contentRecords);
            }
         } // End of outer if that is if logged in user is not community user
         //System.debug('filtertedCaseCommentRecords========='+filtertedCaseCommentRecords);
    }
     public void afterInsert()
     {
        if(Bypasstrigger) return;
         
         //bmm
        List<Case_Monitoring__c> objLstMonitoring = new List<Case_Monitoring__c>();
        List<Case> objLstCase = new List<Case>();

        Case objCase;
        
        boolean IsUserOwner = false;
        Boolean bIsCommunityUser = GetIsCommunityUser();
        // 170619 - Added debug
        //System.debug('bIsCommunityUser======'+bIsCommunityUser);
        // 170619 - Filter case comment records on the basis  of public checkbox
        if(!bIsCommunityUser) {

            // If logged in user is internal user
            for(syn_Case_Comment__c objCaseComment : contentRecords) {

                // Check if isPublished checkbox true
                if(objCaseComment.IsPublished__c) {
                    filtertedCaseCommentRecords.add(objCaseComment);
                }
            } // End of for
            // Send case comment records to service class only if public checkbox is true while insertion
            if(!filtertedCaseCommentRecords.isEmpty()) {

                // 180619 - T - 00275 - Check if triger setting is active
                if(triggerSetting.Is_Case_Comment_Notification_Active__c) {

                    // Pass filtered case comment records
                    CaseCommentService.sendEmailNotificationOnCaseComment(filtertedCaseCommentRecords);
                }
            } // End of if

        } else {

            // 180619 - T - 00275 - Check if triger setting is active
            if(triggerSetting.Is_Case_Comment_Notification_Active__c) {

                // Send all case comment records if logged in user is community user
                
                CaseCommentService.sendEmailNotificationOnCaseComment(contentRecords);
            }
        } // End of if that is logged in user is not community user
        
        // 270619 - T - 00363 - Variable Declarations
        Set<Id> caseIdSet = new Set<Id>();
        Map<Id, List<syn_Case_Comment__c>> CaseIdVSCaseCommentsListMap = new  Map<Id, List<syn_Case_Comment__c>>();
        for(syn_Case_Comment__c obj : contentRecords)
        {
            // Default a case monitoring review for internal users
            boolean bReview = true;
            ID iCreatedById = obj.CreatedById;
            
            //objCase = [SELECT Id, CaseNumber, Status, Case_Communication_Flag__c, OwnerId FROM Case WHERE ID =: obj.Parent_ID__c ];
            
            // 270619 - Commented query
            //objCase = [SELECT Id, CaseNumber, Status, Case_Communication_Flag__c, OwnerId FROM Case WHERE ID =: obj.Case__c ];
            
            // 270619 - T - 00363 - Populate set with case id
            caseIdSet.add(obj.Case__c);
            // 270619 - T - 00363 - Populate CaseIdVSCaseCommentsList map
            if(!CaseIdVSCaseCommentsListMap.containsKey(obj.Case__c)) {
                CaseIdVSCaseCommentsListMap.put(obj.Case__c, new List<syn_Case_Comment__c>{obj});
            } else {
                CaseIdVSCaseCommentsListMap.get(obj.Case__c).add(obj);
            }
           //if (iCreatedById != objCase.OwnerId)

           // 270619 - T - 00363 - Commented code
           /*
           if (bIsCommunityUser)
           {
                objCase.Case_Communication_Flag__c = true;
                objLstCase.add(objCase);
               // set a case monitoring review for community users to false
               bReview = false;
           }        
            
            
            Case_Monitoring__c objMonitoring = new Case_Monitoring__c();
            objMonitoring.Parent_ID__c = obj.ID;
            objMonitoring.Create_Date__c = obj.CreatedDate;
            objMonitoring.Communication_Type__c = 'Case Comment';
            objMonitoring.Name = 'Case:' + objCase.CaseNumber + ' Case Comment';
            objMonitoring.Case__c = objCase.Id;
            // set a case monitoring review based on user 
            objMonitoring.Review_Complete__c = bReview;
            objLstMonitoring.add(objMonitoring);
            */
            // 270619 - T - 00363 - Commented upto here
            
         }
         // 270619 - T - 00363 - Added debug
         //System.debug('CaseIdVSCaseCommentsListMap======'+CaseIdVSCaseCommentsListMap);
         // 270619 - T - 00363 - Fetch all the Case records related with Case Comments
        List<Case> commentsRelatedCaseList = new List<Case>();
        Map<Id,Case> caseIdVSCaseRecMap = new Map<Id,Case>();
        commentsRelatedCaseList = [SELECT Id, CaseNumber, Status, Case_Communication_Flag__c, OwnerId FROM Case WHERE ID IN :caseIdSet];
        for(Case objCaseRec : commentsRelatedCaseList) {
            caseIdVSCaseRecMap.put(objCaseRec.Id,objCaseRec);
        }
        for(Id caseId : CaseIdVSCaseCommentsListMap.keySet()) {
            Case objParentCase = new Case();
            Boolean bReview = true;
            if(caseIdVSCaseRecMap.containsKey(caseId)) {
                objParentCase = caseIdVSCaseRecMap.get(caseId);
            }
            if (bIsCommunityUser){
                objParentCase.Case_Communication_Flag__c = true;
                objLstCase.add(objParentCase);
                // set a case monitoring review for community users to false
                bReview = false;
           } // End of if
           for(syn_Case_Comment__c objCaseComment : CaseIdVSCaseCommentsListMap.get(caseId)) {
                if(objParentCase != null) {
                    Case_Monitoring__c objMonitoring = new Case_Monitoring__c();
                    objMonitoring.Parent_ID__c = objCaseComment.ID;
                    objMonitoring.Create_Date__c = objCaseComment.CreatedDate;
                    objMonitoring.Communication_Type__c = 'Case Comment';
                    objMonitoring.Name = 'Case:' + objParentCase.CaseNumber + ' Case Comment';
                    objMonitoring.Case__c = objParentCase.Id;
                    // set a case monitoring review based on user 
                    objMonitoring.Review_Complete__c = bReview;
                    objLstMonitoring.add(objMonitoring);
                } // End of if
           } // End of inner for
        } // End of outer for
        // upto here    
        
        // 270619 - T - 00363 - Added debug
        //System.debug('objLstMonitoring========'+objLstMonitoring);
        // 270619 - T - 00363 - Added if condition
        if(!objLstMonitoring.isEmpty()) {
            insert objLstMonitoring;
        }
         
       if (objLstCase.size()>0)
       {
            update objLstCase;  
       }

        //CaseCommentService.updateCaseMonitoringRecords(CaseCommentService.getCaseTypeCaseCommentRecords(contentRecords));
     }

    
    public static boolean GetIsCommunityUser () 
    {
        //
        return (getPathPrefix() != '' ? true : false);
        //
        
    }

    public static String getPathPrefix () {
        Id net_id = Network.getNetworkId();
        if (net_id  != null) {
            return [SELECT Id, UrlPathPrefix
                    FROM Network
                    WHERE Id = :net_id].UrlPathPrefix;
        }
        return '';
    }

}