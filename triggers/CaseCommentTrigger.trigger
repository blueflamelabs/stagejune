trigger CaseCommentTrigger on syn_Case_Comment__c (before insert,after insert, after update)
{
    CaseCommentHandler obj= new CaseCommentHandler(Trigger.new);
    
    Switch on Trigger.operationType
    {

        when BEFORE_INSERT 
        {
            //obj.beforeInsert();
        }
        when AFTER_INSERT 
        {
            //System.debug('After insert called');
            obj.afterInsert();
        }
        when AFTER_UPDATE 
        {
            //System.debug('After update called');
            // obj.afterUpdate();
            // 170619 - Pass list of case comment records and Trigger.OldMap
            CaseCommentHandler objHandler = new CaseCommentHandler(Trigger.new, Trigger.oldMap);
            objHandler.afterUpdate();
        }
        
    }


}