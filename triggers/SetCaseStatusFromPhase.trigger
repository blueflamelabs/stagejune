trigger SetCaseStatusFromPhase on Change_Management_Phase__c (before update) 
{
    // this is not great bulk coding. fix later
    //
    
    //Stop any edits after status is equal to complete
    //
    //
    Profile p = [select id,Name from Profile where id=:Userinfo.getProfileid()];
  	//if((offerObject.RecordType.DeveloperName == 'ABC) && (p.Name  == 'System Administrator'))
   
    for(Change_Management_Phase__c objPhase : trigger.new)
    {
        if ((Trigger.oldmap.get(objPhase.id).Status__c == 'Complete') && (p.Name  != 'System Administrator'))
        {
           objPhase.addError('This phase has been completed, and cannot be edited. Contact the system administrator if you need help.');
           return;      
        }    
    }

    Boolean bReturn = false;
    
    for(Change_Management_Phase__c cs : trigger.new)
    {
        if (cs.Timing_Required__c == null)
        {
           // cs.addError('The Timing Required field cannot be empty.');
           // bReturn = true;        
        }
        if (cs.Date_Required__c == null)
        {
            //cs.addError('The Date Required field cannot be empty.');
           //bReturn = true;        
        }
        if (cs.Change_Instructions__c == null)
        {
            cs.addError('The Change Instructions field cannot be empty.');
            bReturn = true;        
        }
        if (cs.Change_Application__c == null)
        {
            cs.addError('The Change Application field cannot be empty.');
            bReturn = true;        
        }
    
    }
    if (bReturn == true )
    {
        return;
    }
     
    Id objCaseId;
    Id objOwnerId;
    String strPhaseStatus;
    String strNewCaseStatus = 'New';
    Boolean bUpDateOwner = false;
    Boolean bUpDateCase = false;
    
    for(Change_Management_Phase__c obj : trigger.new)
    {
        if (Trigger.oldmap.get(obj.id).Status__c != Trigger.newmap.get(obj.id).Status__c)
        {

            //Id objCaseId  = obj.Case__c;
            //String strPhaseStatus = obj.Status__c;
           // String strNewCaseStatus = 'New';
 
            objCaseId  = obj.Case__c;
            objOwnerId = obj.OwnerId;
            strPhaseStatus = obj.Status__c;
            strNewCaseStatus = 'New';

            Case objCase = [select id, status, OwnerId, CreatedById from Case where id = :objCaseId];

            
            if (strPhaseStatus == 'Initial Review Approval Pending')
            {
                strNewCaseStatus = 'Initial Change Approval';
                bUpDateCase = true;
            }
            else if (strPhaseStatus == 'Initial Review Approval')
            {
                strNewCaseStatus = 'Initial Change Approval';  
                bUpDateCase = true;
            }
            else if (strPhaseStatus == 'Initial Review Approval Rejected')
            {
                strNewCaseStatus = 'New';
                objOwnerId = objCase.CreatedById;
                bUpDateOwner = true;
                bUpDateCase = true;
            }
            
            if (strPhaseStatus == 'Emergency Approval Pending')
            {
                strNewCaseStatus = 'Emergency Change Approval';  
                bUpDateCase = true;
            }
            else if (strPhaseStatus == 'Emergency Approval')
            {
                strNewCaseStatus = 'Emergency Change Approval';  
                bUpDateCase = true;
            }
            else if (strPhaseStatus == 'Emergency Approval Rejected')
            {
                strNewCaseStatus = 'New';
                objOwnerId = objCase.CreatedById;
                bUpDateOwner = true;
                bUpDateCase = true;
            }
            
            if (strPhaseStatus == 'Production Implementation Approval Pending')
            {
                strNewCaseStatus = 'Production Change Verification';  
                bUpDateCase = true;
            }
            else if (strPhaseStatus == 'Production Implementation Approval')
            {
                strNewCaseStatus = 'Production Change Verification';        //Leave here for submitter to pick next stage. Add New Phase will fire process and reset to new and create a new phase
                bUpDateCase = true;
            }
            else if (strPhaseStatus == 'Production Implementation Approval Rejected')
            {
                strNewCaseStatus = 'Production Implementation';  
                bUpDateCase = true;
            }
            if (strPhaseStatus == 'Complete')
            {
            
            }
            
            if (strPhaseStatus == 'Ops General Approval Pending')
            {
                strNewCaseStatus = 'Ops General Approval';  
                bUpDateCase = true;
            }
            else if (strPhaseStatus == 'Ops General Approval')
            {
                strNewCaseStatus = 'Ops General Approval';  
                bUpDateCase = true;
            }
            else if (strPhaseStatus == 'Ops General Approval Rejected')
            {
                strNewCaseStatus = 'New';  
                bUpDateCase = true;
            }
            
            //if (1==2)
            //{
                //ProcessInstance objProcessInstance = [select id, status, targetobjectid from processinstance where targetobjectid = :obj.id];
                //ProcessInstanceStep objProcessInstanceStep = [select id, comments, processinstanceid, stepstatus, actorid, originalactorid from processinstancestep where processinstanceid = :objProcessInstance.Id];
                
               // User objUser = [select id from User where id = :objProcessInstanceStep.actorid];
            //}
            //objCase.Status = strNewCaseStatus;
            //update objCase;
        }
    }

    if (strPhaseStatus == 'Complete')
    {
        // do nothing for now
    }
    else
    {    
        if (bUpDateCase == true)
        {
        List<Case> lstCase = [select id, status, OwnerId, CreatedById from Case where id = :objCaseId];
        if (lstCase[0].Status != strNewCaseStatus)
        {
            //System.debug('in status not equal');
            lstCase[0].Status = strNewCaseStatus;
            if (bUpDateOwner == true)
            {
                lstCase[0].OwnerId = objOwnerId;    
            }
            //
            if (bUpDateCase == true)
            {
                update lstCase[0];
            }
        }
            }
    
    }

}